﻿using FluentValidation;
using MediatR;
using Permissions.Application.Queries.GetPermissionList.Query;
using Permissions.Application.Queries.GetPermissionList.Response;
using Permissions.Domain.Permission.Services.GetList;

namespace Permissions.Application.Queries.GetPermissionList
{
    public class GetPermissionListQueryHandler : IRequestHandler<GetPermissionListQuery, PermissionListResponse>
    {
        private readonly IValidator<GetPermissionListQuery> validator;
        private readonly IGetPermissionListService getPermissionListDomainService;

        public GetPermissionListQueryHandler(
            IValidator<GetPermissionListQuery> validator,
            IGetPermissionListService getPermissionListDomainService)
        {
            this.validator = validator;
            this.getPermissionListDomainService = getPermissionListDomainService;
        }

        public Task<PermissionListResponse> Handle(GetPermissionListQuery request, CancellationToken cancellationToken)
        {
            this.validator.ValidateAndThrow(request);
            var permissions = this.getPermissionListDomainService.Execute();
            var response = PermissionResponseMapper.Map(permissions);

            return Task.FromResult(response);
        }
    }
}
