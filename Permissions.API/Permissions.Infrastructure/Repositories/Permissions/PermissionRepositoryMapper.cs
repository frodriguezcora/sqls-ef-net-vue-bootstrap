﻿using Permissions.Domain.Permission;
using Permissions.Infrastructure.Models;

namespace Permissions.Infrastructure.Repositories.Permissions
{
    internal static class PermissionRepositoryMapper
    {
        internal static PermissionModel MapEntityToModel(Permission permission) => new()
        {
            Id = permission.Id?.Value ?? 0,
            TypeId = permission.Type.Id.Value,
            EmployeeName = permission.EmployeeName,
            EmployeeSurname = permission.EmployeeSurname,
            RequestDate = permission.RequestDate
        };

        internal static Permission MapModelToEntity(PermissionModel permissionModel) => new()
        {
            Id = new(permissionModel.Id),
            Type = new()
            {
                Id = new(permissionModel.Type.Id),
                Description = permissionModel.Type.Description
            },
            EmployeeName = permissionModel.EmployeeName,
            EmployeeSurname = permissionModel.EmployeeSurname,
            RequestDate = permissionModel.RequestDate
        };
    }
}
