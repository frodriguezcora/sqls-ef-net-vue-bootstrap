﻿using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission
{
    public class PermissionId : IEntityId
    {
        public PermissionId(int id)
        {
            this.Value = id;
        }

        public int Value { get; private set; }
    }
}
