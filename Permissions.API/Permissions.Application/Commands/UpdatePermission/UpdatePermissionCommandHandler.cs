using FluentValidation;
using MediatR;
using Permissions.Application.Commands.UpdatePermission.Command;
using Permissions.Domain.Permission.Builder;
using Permissions.Domain.Permission.Services.Update;

namespace Permissions.Application.Commands.UpdatePermission;

public class UpdatePermissionCommandHandler : IRequestHandler<UpdatePermissionCommand>
{
    private readonly IValidator<UpdatePermissionCommand> validator;
    private IPermissionBuilder permissionBuilder;
    private IUpdatePermissonService updatePermissonService;

    public UpdatePermissionCommandHandler(
        IValidator<UpdatePermissionCommand> validator,
        IPermissionBuilder permissionBuilder,
        IUpdatePermissonService updatePermissonService)
    {
        this.validator = validator;
        this.permissionBuilder = permissionBuilder;
        this.updatePermissonService = updatePermissonService;
    }

    public Task<Unit> Handle(UpdatePermissionCommand command, CancellationToken cancellationToken)
    {
        this.validator.ValidateAndThrow(command);

        var instance = this.permissionBuilder
        .SetId(new(command.PermissionId))
        .SetType(new(command.PermissionTypeId))
        .SetRequestDate(command.RequestDate)
        .SetEmployeeName(command.EmployeeName)
        .SetEmployeeSurname(command.EmployeeSurname)
        .Build();

        this.updatePermissonService.Execute(instance);

        return Task.FromResult(Unit.Value);
    }
}
