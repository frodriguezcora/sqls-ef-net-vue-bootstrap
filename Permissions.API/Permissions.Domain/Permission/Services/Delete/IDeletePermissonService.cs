namespace Permissions.Domain.Permission.Services.Delete;

public interface IDeletePermissonService
{
    void Execute(PermissionId permissionId);
}