﻿namespace Permissions.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(PermissionsDbContext context)
        {
            this.Context = context;
        }

        public PermissionsDbContext Context { get; }

        public void Commit()
        {
            this.Context.SaveChanges();
        }

        public void Dispose()
        {
            this.Context.Dispose();
        }
    }
}
