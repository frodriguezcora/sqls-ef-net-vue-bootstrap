import axios from 'axios'

const httpClient = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  withCredentials: true,
  timeout: 5000,
  headers: {
    'Context-Type': 'application/json'
  }
})

export default httpClient