﻿using Permissions.Infrastructure;
using Permissions.Infrastructure.UnitOfWork;

namespace Permissions.API.Middlewares
{
    public class UnitOfWorkMiddleware
    {
        private RequestDelegate next;

        public UnitOfWorkMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, IUnitOfWork unitOfWork)
        {
            await next.Invoke(httpContext);
            unitOfWork.Commit();
            unitOfWork.Dispose();
        }
    }
}
