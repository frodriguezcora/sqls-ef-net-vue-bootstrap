using FluentValidation;
using MediatR;
using Permissions.Application.Commands.DeletePermission.Command;
using Permissions.Domain.Permission.Services.Delete;

namespace Permissions.Application.Commands.DeletePermission;

public class DeletePermissionCommandHandler : IRequestHandler<DeletePermissionCommand>
{
    private readonly IValidator<DeletePermissionCommand> validator;
    private IDeletePermissonService deletePermissonService;

    public DeletePermissionCommandHandler(
        IValidator<DeletePermissionCommand> validator,
        IDeletePermissonService deletePermissonService)
    {
        this.validator = validator;
        this.deletePermissonService = deletePermissonService;
    }

    public Task<Unit> Handle(DeletePermissionCommand command, CancellationToken cancellationToken)
    {
        this.validator.ValidateAndThrow(command);
        this.deletePermissonService.Execute(new(command.PermissionId));

        return Task.FromResult(Unit.Value);
    }
}
