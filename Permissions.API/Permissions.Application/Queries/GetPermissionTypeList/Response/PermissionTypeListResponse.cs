﻿namespace Permissions.Application.Queries.GetPermissionTypeList.Response
{
    public class PermissionTypeListResponse
    {
        public IEnumerable<PermissionTypeDto> List { get; set; }
    }
}