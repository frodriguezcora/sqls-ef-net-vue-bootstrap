﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Permissions.Application.Commands.CreatePermission.Command;
using Permissions.Application.Commands.DeletePermission.Command;
using Permissions.Application.Commands.UpdatePermission.Command;
using Permissions.Application.Queries.GetPermissionList.Query;

namespace Permissions.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionController : Controller
    {
        private ISender sender;

        public PermissionController(ISender sender)
        {
            this.sender = sender;
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreatePermissionCommand command)
        {
            var response = await this.sender.Send(command);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this.sender.Send(new DeletePermissionCommand
            {
                PermissionId = id
            });

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, UpdatePermissionCommand command)
        {
            command.PermissionId = id;
            await this.sender.Send(command);

            return Ok();
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> Get()
        {
            var response = await this.sender.Send(new GetPermissionListQuery());

            return Ok(response);
        }
    }
}
