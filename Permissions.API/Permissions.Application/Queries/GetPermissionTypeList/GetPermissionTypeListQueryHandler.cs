﻿using FluentValidation;
using MediatR;
using Permissions.Application.Queries.GetPermissionTypeList.Query;
using Permissions.Application.Queries.GetPermissionTypeList.Response;
using Permissions.Domain.Permission.Services.GetTypeList;

namespace Permissions.Application.Queries.GetPermissionTypeList
{
    public class GetPermissionTypeListQueryHandler : IRequestHandler<GetPermissionTypeListQuery, PermissionTypeListResponse>
    {
        private readonly IValidator<GetPermissionTypeListQuery> validator;
        private readonly IGetPermissionTypeListService getPermissionTypeListDomainService;

        public GetPermissionTypeListQueryHandler(
            IValidator<GetPermissionTypeListQuery> validator,
            IGetPermissionTypeListService getPermissionTypeListDomainService)
        {
            this.validator = validator;
            this.getPermissionTypeListDomainService = getPermissionTypeListDomainService;
        }

        public Task<PermissionTypeListResponse> Handle(GetPermissionTypeListQuery request, CancellationToken cancellationToken)
        {
            this.validator.ValidateAndThrow(request);
            var permissions = this.getPermissionTypeListDomainService.Execute();
            var response = PermissionTypeListResponseMapper.Map(permissions);

            return Task.FromResult(response);
        }
    }
}
