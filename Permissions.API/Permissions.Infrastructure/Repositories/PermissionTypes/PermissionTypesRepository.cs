﻿using Permissions.Domain.Contract;
using Permissions.Domain.Permission.Type;
using Permissions.Infrastructure.Models;
using Permissions.Infrastructure.Repositories.Permissions;
using Permissions.Infrastructure.UnitOfWork;

namespace Permissions.Infrastructure.Repositories.PermissionTypes
{
    public class PermissionTypesRepository : BaseModelRepository<PermissionTypeModel>, IRedableRepository<PermissionType>
    {
        public PermissionTypesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public PermissionType GetById(IEntityId id)
        {
            var model = this.GetModelById(id.Value);
            return PermissionTypeRepositoryMapper.MapModelToEntity(model);
        }

        public IEnumerable<PermissionType> GetList()
        {
            var models = this.GetModelList();
            return models.Select(model => PermissionTypeRepositoryMapper.MapModelToEntity(model));
        }
    }
}
