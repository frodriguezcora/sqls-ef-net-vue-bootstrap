﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Permissions.Application.Queries.GetPermissionTypeList.Query;

namespace Permissions.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionTypeController : Controller
    {
        private ISender sender;

        public PermissionTypeController(ISender sender)
        {
            this.sender = sender;
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> Get()
        {
            var response = await this.sender.Send(new GetPermissionTypeListQuery());

            return Ok(response);
        }
    }
}
