# NET-EF-VUE-BOOTSTRAP

## SPECIFICATION
### Base Datos
Crear una tabla con el nombre “Permiso” y los siguientes campos:

|  Nombre  | Tipo de dato  | Extra  | Definición |
| ------------ | ------------ | ------------ | ------------ |
| Id  | Entero | autoincrementable | Identificador único del registro |
| Nombre Empleado | Texto | No permite nulo |  Nombre del Empleado |
| Apellidos Empleado | Texto | No permite nulo |  Apellidos del Empleado |
| Tipo Permiso | Entero | No permite nulo |  Tipo del permiso solicitado |


Crear una tabla con el nombre “TipoPermiso” y los siguientes campos:

|  Nombre  | Tipo de dato  | Extra  | Definición |
| ------------ | ------------ | ------------ | ------------ |
| Id | Entero | autoincrementable |  Identificador único del registro |
| Descripción | Texto | No permite nulo | Descripción del permiso |


### Backend
1. Crear una aplicación web en Visual Studio, utilizando asp.net core (3.1 en adelante).
2. No utilizar scaffolding para crear la conexión entre base de datos y la aplicación.
3. Utilizar EntityFramework para manejo de datos.

### Front End:
1. Crear proyecto Node.js
2. Usar un framework CSS (bootsrap)
3. Usar un framework javascript para el front end (Vue.js).

##### Vista 1: Insertar y Editar
1. Los campos que no permiten nulo deben ser validados.
2. Campos a entrar datos:
a. Nombre
b. Apellido
c. Tipo Permiso
d. Fecha Solicitud Permiso

##### Vista 2: Listar Permisos Registrados
1. Mostrar todos los permisos registrados
2. Eliminar récord de la tabla Permiso
3. Incluir mensaje de confirmación al eliminar un permiso


## 1- Clone The Repository

```
git clone https://gitlab.com/frodriguezcora/sqls-ef-net-vue-bootstrap.git

It's public, don't worry

```

## 2- Execute The SQL script

```
Execute the DataBaseScript.sql in SQL Server to create the database and populate tables
```

## 3- Continue With Setups Steps

- [ ] [Front Application Setup](https://gitlab.com/frodriguezcora/sqls-ef-net-vue-bootstrap/-/blob/main/Permissions.UI/README.md)
- [ ] [Back Application Setup](https://gitlab.com/frodriguezcora/sqls-ef-net-vue-bootstrap/-/blob/main/Permissions.API/README.md)