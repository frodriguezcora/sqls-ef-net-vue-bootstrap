using FluentValidation;

namespace Permissions.Application.Commands.CreatePermission.Command
{
    public class CreatePermissionCommandValidator : AbstractValidator<CreatePermissionCommand>
    {
        public CreatePermissionCommandValidator()
        {
            RuleFor(x => x.PermissionTypeId).NotEmpty();
            RuleFor(x => x.EmployeeName).NotEmpty();
            RuleFor(x => x.EmployeeSurname).NotEmpty();
            RuleFor(x => x.RequestDate).GreaterThanOrEqualTo(p => DateTime.Today);
        }
    }
}