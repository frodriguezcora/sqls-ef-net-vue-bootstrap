# Front Application

## 1- Install Dependencies
```
npm install
```

### 2- Prepare .env file
```
Rename .env.dist file to .env
Set value to VUE_APP_API_BASE_URL (example: VUE_APP_API_BASE_URL=https://localhost:XXXX/)
```

### 3- Start dev server
```
npm run serve
```