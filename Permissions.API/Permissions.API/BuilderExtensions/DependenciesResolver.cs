﻿using FluentValidation;
using MediatR;
using Permissions.Domain.Contract;
using Permissions.Domain.Permission;
using Permissions.Domain.Permission.Type;
using Permissions.Infrastructure.Repositories.Permissions;
using Permissions.Infrastructure.Repositories.PermissionTypes;
using System.Reflection;

namespace Permissions.API.BuilderExtensions
{
    public static class DependenciesResolver
    {
        private const string MediatorDependencies = "Permissions.Application";
        private const string ValidatorDependencies = "Permissions.Application";

        private static IEnumerable<string> ScopedDependencies = new[]
        {
            "Permissions.Application",
            "Permissions.Infrastructure"
        };

        private static IEnumerable<string> TransientDependencies = new[]
        {
            "Permissions.Domain",
        };

        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.Load(MediatorDependencies));
            services.AddValidatorsFromAssembly(Assembly.Load(ValidatorDependencies));

            ResolveScopedDependencies(services);
            ResolveTransientDependencies(services);

            return services;
        }

        private static void ResolveScopedDependencies(IServiceCollection services)
        {
            var asemblies = ScopedDependencies.Select(namespaceString => Assembly.Load(namespaceString));

            services.Scan(scan =>
                scan.FromAssemblies(asemblies)
                    .AddClasses()
                    .AsMatchingInterface()
                    .WithScopedLifetime());

            services.AddScoped<IRedableRepository<Permission>, PermissionRepository>();
            services.AddScoped<IWritableRepository<Permission>, PermissionRepository>();
            services.AddScoped<IRedableRepository<PermissionType>, PermissionTypesRepository>();
        }

        private static void ResolveTransientDependencies(IServiceCollection services)
        {
            var asemblies = TransientDependencies.Select(namespaceString => Assembly.Load(namespaceString));

            services.Scan(scan =>
                scan.FromAssemblies(asemblies)
                    .AddClasses()
                    .AsMatchingInterface()
                    .WithScopedLifetime());
        }

    }
}
