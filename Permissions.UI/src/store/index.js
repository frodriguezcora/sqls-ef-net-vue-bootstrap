import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import Permission from '@/store/entities/Permission'
import PermissionType from '@/store/entities/PermissionType'

import {
  LOAD_PERMISSIONS,
  LOAD_PERMISSION_TYPES,
  CREATE_PERMISSION,
  EDIT_PERMISSION,
  REMOVE_PERMISSION } from '@/store/actions'

import {
  STOP_LOADING,
  START_LOADING,
  START_ERROR,
  STOP_ERROR,
  SET_PERMISSIONS,
  SET_PERMISSION_TYPE,
  ADD_PERMISSION,
  REPLACE_PERMISSION,
  DELETE_PERMISSION } from '@/store/mutations'

import PermissionApi from '@/services/permission-api'
import PermissionTypeApi from '@/services/permission-type-api'

const actionHanlder = async (commit, actionCallback) => {
  commit(START_LOADING)

  try {
    await actionCallback()
  }
  catch(error) {
    commit(START_ERROR)
  }

  commit(STOP_LOADING)
}

export default new Vuex.Store({
    state: {
      loading: false,
      error: false,
      permissions: [],
      permissionTypes: [],
    },

    actions: {
      async [LOAD_PERMISSIONS]({ commit }) {
        actionHanlder(commit, async () => 
        {
          const response = await PermissionApi.getList()
          const permissions = response.data.list
          commit(SET_PERMISSIONS, permissions)
        })
      },

      async [LOAD_PERMISSION_TYPES]({ commit }) {
        actionHanlder(commit, async () => 
        {
          const response = await PermissionTypeApi.getList()
          const permissionTypes = response.data.list
          commit(SET_PERMISSION_TYPE, permissionTypes)
        })
      },

      async [CREATE_PERMISSION]({ commit }, payload) {
        actionHanlder(commit, async () => 
        {
          const response = await PermissionApi.create(payload)
          payload.id = response.data.id
          commit(ADD_PERMISSION, payload)
        })
      },

      async [EDIT_PERMISSION]({ commit }, payload) {
        actionHanlder(commit, async () => 
        {
          await PermissionApi.update(payload)
          commit(REPLACE_PERMISSION, payload)
        })
      },

      async [REMOVE_PERMISSION]({ commit }, id) {
        actionHanlder(commit, async () => 
        {
          await PermissionApi.delete(id)
          commit(DELETE_PERMISSION, id)
        })
      },
    },
    
    mutations: {
      [START_LOADING](state) {
        state.loading = true
      },
      [STOP_LOADING](state) {
        state.loading = false
      },
      [START_ERROR](state) {
        state.error = true
      },
      [STOP_ERROR](state) {
        state.error = false
      },
      [SET_PERMISSIONS](state, permissions) {
        state.permissions = permissions.map(raw => new Permission(
          raw.employeeName,
          raw.employeeSurname,
          raw.permissionTypeId,
          raw.requestDate,
          raw.permissionId
        ))
      },
      [SET_PERMISSION_TYPE](state, permissionTypes) {
        state.permissionTypes = permissionTypes.map(raw => new PermissionType(
          raw.id,
          raw.description
        ))
      },
      [ADD_PERMISSION](state, permission) {
        state.permissions.push(permission)
      },
      [REPLACE_PERMISSION](state, permission) {
        let permissionsToKeep = state.permissions.filter(permissionToKeep => permissionToKeep.id != permission.id)
        permissionsToKeep.push(permission)
        state.permissions = permissionsToKeep
      },
      [DELETE_PERMISSION](state, id) {
        let permissionsToKeep = state.permissions.filter(permissionToKeep => permissionToKeep.id != id)
        state.permissions = permissionsToKeep
      },
    },
    
    getters: {
      isLoading(state) {
        return state.loading
      },
      hasError(state) {
        return state.error
      },
      permissions(state) {
        return state.permissions
      },
      permissionTypes(state) {
        return state.permissionTypes
      },
    },
})