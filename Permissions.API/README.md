# Backend Application : Environment Variables (User Secrets)

## 1- Initialize User Secrets
```
dotnet user-secrets init --project Permissions.API
```

### 2- Set "SqlServerConnectionString" variable value
```
dotnet user-secrets set "SqlServerConnectionString" "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=myPassword;" --project Permissions.API

If you have problems with special chars remember use "" to scape special chars

Example Command
dotnet user-secrets set "SqlServerConnectionString" "Server=localhost""\\""XXXX;Database=DEV_Permissions;Trusted_Connection=True;" 

FRIENDLY REMEMBER: the database name to this example is DEV_Permissions

```

### 3- Run dev server
```
dotnet run --project Permissions.API
```
