using MediatR;
using System.Text.Json.Serialization;

namespace Permissions.Application.Commands.UpdatePermission.Command;

public class UpdatePermissionCommand : IRequest
{
    [JsonIgnore]
    public int PermissionId { get; set; }

    public int PermissionTypeId { get; set; }

    public string EmployeeName { get; set; }

    public string EmployeeSurname { get; set; }

    public DateTime RequestDate { get; set; }
}