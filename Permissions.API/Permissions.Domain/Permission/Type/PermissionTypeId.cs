﻿using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Type
{
    public class PermissionTypeId : IEntityId
    {
        public PermissionTypeId(int id)
        {
            this.Value = id;
        }

        public int Value { get; private set; }
    }
}
