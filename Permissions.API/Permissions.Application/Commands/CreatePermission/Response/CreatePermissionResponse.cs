﻿namespace Permissions.Application.Commands.CreatePermission.Response
{
    public class CreatePermissionResponse
    {
        public int Id { get; set; }
    }
}
