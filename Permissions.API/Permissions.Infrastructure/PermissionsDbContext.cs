﻿using Microsoft.EntityFrameworkCore;
using Permissions.Infrastructure.Models;

namespace Permissions.Infrastructure
{
    public class PermissionsDbContext : DbContext
    {
        public DbSet<PermissionModel> Permission { get; set; }

        public DbSet<PermissionTypeModel> PermissionType { get; set; }

        public PermissionsDbContext(DbContextOptions<PermissionsDbContext> options) : base(options)
        {
        }
    }
}
