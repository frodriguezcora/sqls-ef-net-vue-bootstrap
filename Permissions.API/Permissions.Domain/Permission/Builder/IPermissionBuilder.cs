﻿using Permissions.Domain.Permission.Type;

namespace Permissions.Domain.Permission.Builder
{
    public interface IPermissionBuilder
    {
        IPermissionBuilder SetId(PermissionId id);

        IPermissionBuilder SetEmployeeName(string name);

        IPermissionBuilder SetEmployeeSurname(string surname);

        IPermissionBuilder SetType(PermissionTypeId typeId);

        IPermissionBuilder SetRequestDate(DateTime requestDate);

        Permission Build();
    }
}
