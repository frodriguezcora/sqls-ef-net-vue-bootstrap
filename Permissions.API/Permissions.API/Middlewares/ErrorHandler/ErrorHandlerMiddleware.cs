﻿using FluentValidation;
using System.Net;
using System.Text.Json;

namespace Permissions.API.Middlewares.ErrorHandler
{
    public class ErrorHandlerMiddleware
    {
        private RequestDelegate next;
        private ILogger<ErrorHandlerMiddleware> logger;

        public ErrorHandlerMiddleware(
            RequestDelegate next,
            ILogger<ErrorHandlerMiddleware> logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            var errorResponse = new ErrorResponse
            {
                Message = string.Empty
            };
                

            if (exception is ValidationException)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                errorResponse.Message = exception.Message;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                errorResponse.Message = "Unexpected Error";
            }

            logger.LogCritical(exception, exception.Message, context);
            return context.Response.WriteAsync(JsonSerializer.Serialize(errorResponse));
        }
    }
}
