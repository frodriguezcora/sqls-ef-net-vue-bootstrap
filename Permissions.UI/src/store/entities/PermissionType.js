export default class PermissionType {

    constructor(
        id,
        name)
    {
        this.id = id
        this.name = name
    }
}