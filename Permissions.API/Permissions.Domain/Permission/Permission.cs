﻿using Permissions.Domain.Contract;
using Permissions.Domain.Permission.Type;

namespace Permissions.Domain.Permission
{
    public class Permission : IEntity
    {
        public PermissionId Id { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeSurname { get; set; }

        public PermissionType Type { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
