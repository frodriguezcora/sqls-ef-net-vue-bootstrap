﻿using Permissions.Domain.Permission;

namespace Permissions.Application.Queries.GetPermissionList.Response
{
    public static class PermissionResponseMapper
    {
        public static PermissionListResponse Map(IEnumerable<Permission> permissions) => new()
        {
            List = permissions.Select(permission => new PermissionDto
            {
                PermissionId = permission.Id.Value,
                PermissionTypeId = permission.Type.Id.Value,
                PermissionTypeDescription = permission.Type.Description,
                EmployeeName = permission.EmployeeName,
                EmployeeSurname = permission.EmployeeSurname,
                RequestDate = permission.RequestDate
            })
        };
    }
}
