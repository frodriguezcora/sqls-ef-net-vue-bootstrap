import httpClient from '@/services'

export default new class PermissionApi {

    constructor() {
        this.basePath = 'permission'
    }

    create(payload) {
        return httpClient.post(`${this.basePath}`, 
        {
            permissionTypeId: payload.permissionTypeId,
            employeeName: payload.name,
            employeeSurname: payload.surname,
            requestDate: payload.requestDate,
        })
    }

    update(payload) {
        return httpClient.put(`${this.basePath}/${payload.id}`, 
        {
            permissionTypeId: payload.permissionTypeId,
            employeeName: payload.name,
            employeeSurname: payload.surname,
            requestDate: payload.requestDate,
        })
    }

    delete(id) {
        return httpClient.delete(`${this.basePath}/${id}`)
    }

    getList() {
        return httpClient.get(`${this.basePath}/list`)
    }
}