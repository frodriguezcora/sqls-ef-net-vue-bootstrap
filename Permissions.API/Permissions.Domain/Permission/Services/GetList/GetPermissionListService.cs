using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Services.GetList;

public class GetPermissionListService : IGetPermissionListService
{
    private IRedableRepository<Permission> redablePermissionRepository;

    public GetPermissionListService(IRedableRepository<Permission> redablePermissionRepository)
    {
        this.redablePermissionRepository = redablePermissionRepository;
    }

    public IEnumerable<Permission> Execute()
    {
        return this.redablePermissionRepository.GetList();
    }
}
