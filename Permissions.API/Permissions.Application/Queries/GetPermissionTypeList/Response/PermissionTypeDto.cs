﻿namespace Permissions.Application.Queries.GetPermissionTypeList.Response
{
    public class PermissionTypeDto
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
