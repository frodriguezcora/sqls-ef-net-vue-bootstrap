using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Services.Delete;

public class DeletePermissonService : IDeletePermissonService
{
    private IWritableRepository<Permission> writablePermissionRepository;

    public DeletePermissonService(IWritableRepository<Permission> writablePermissionRepository)
    {
        this.writablePermissionRepository = writablePermissionRepository;
    }

    public void Execute(PermissionId permissionId)
    {
        this.writablePermissionRepository.Delete(permissionId);
    }
}
