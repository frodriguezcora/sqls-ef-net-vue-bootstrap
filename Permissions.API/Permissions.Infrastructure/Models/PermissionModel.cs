﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Permissions.Infrastructure.Models
{
    public class PermissionModel : IModel
    {
        [Key]
        public int Id { get; set; }

        public int TypeId { get; set; }

        [ForeignKey(nameof(TypeId))]
        public virtual PermissionTypeModel Type { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeSurname { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
