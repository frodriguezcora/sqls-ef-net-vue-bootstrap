CREATE DATABASE DEV_Permissions;

GO
USE [DEV_Permissions]
GO

CREATE TABLE PermissionType(
	Id INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Description VARCHAR(100) NOT NULL
);
GO

CREATE TABLE Permission (
	Id INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	TypeId INTEGER NOT NULL,
	EmployeeName VARCHAR(50) NOT NULL,
	EmployeeSurname VARCHAR(50) NOT NULL,
	RequestDate DATETIME NOT NULL,
	FOREIGN KEY (TypeId) REFERENCES PermissionType(Id)
);
GO

INSERT INTO PermissionType (Description)
VALUES ('Enfermedad'), ('Diligencias'), ('Otros');