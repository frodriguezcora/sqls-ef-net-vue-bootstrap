﻿using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Type
{
    public class PermissionType : IEntity
    {
        public PermissionTypeId Id { get; set; }

        public string Description { get; set; }
    }
}
