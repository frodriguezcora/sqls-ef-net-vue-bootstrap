﻿namespace Permissions.Domain.Contract
{
    public interface IEntityId
    {
        int Value { get; }
    }
}
