﻿using Permissions.Domain.Contract;
using Permissions.Domain.Permission.Type;

namespace Permissions.Domain.Permission.Builder
{
    public class PermissionBuilder : IPermissionBuilder
    {
        private const string InvalidPermission = "The permission build throw an unexpected error";

        private IRedableRepository<PermissionType> redablePermissionTypeRepository;

        private PermissionId? Id { get; set; }
        private string? EmployeeName { get; set; }
        private string? EmployeeSurname { get; set; }
        private DateTime? RequestDate { get; set; }
        public PermissionType? Type { get; set; }

        public PermissionBuilder(IRedableRepository<PermissionType> redablePermissionTypeRepository)
        {
            this.redablePermissionTypeRepository = redablePermissionTypeRepository;
        }

        public IPermissionBuilder SetId(PermissionId id)
        {
            this.Id = id;
            return this;
        }

        public IPermissionBuilder SetEmployeeName(string name)
        {
            this.EmployeeName = name;
            return this;
        }

        public IPermissionBuilder SetEmployeeSurname(string surname)
        {
            this.EmployeeSurname = surname;
            return this;
        }

        public IPermissionBuilder SetRequestDate(DateTime requestDate)
        {
            this.RequestDate = requestDate.ToUniversalTime();
            return this;
        }

        public IPermissionBuilder SetType(PermissionTypeId typeId)
        {
            this.Type = this.redablePermissionTypeRepository.GetById(typeId);
            return this;
        }

        private void ValidateAndThrow()
        {
            if (string.IsNullOrWhiteSpace(this.EmployeeName))
            {
                throw new InvalidDataException(InvalidPermission);
            }

            if (string.IsNullOrWhiteSpace(this.EmployeeSurname))
            {
                throw new InvalidDataException(InvalidPermission);
            }

            if (this.RequestDate is null)
            {
                throw new InvalidDataException(InvalidPermission);
            }

            if (this.Type is null)
            {
                throw new InvalidDataException(InvalidPermission);
            }
        }

        private void Reset()
        {
            this.Id = null;
            this.EmployeeName = string.Empty;
            this.EmployeeSurname = string.Empty;
            this.RequestDate = null;
            this.Type = null;
        }

        public Permission Build()
        {
            this.ValidateAndThrow();
            var instance = new Permission
            {
                EmployeeName = this.EmployeeName ?? String.Empty,
                EmployeeSurname = this.EmployeeSurname ?? String.Empty,
                RequestDate = this.RequestDate ?? DateTime.Today,
                Type = this.Type
            };

            if (this.Id is not null)
            {
                instance.Id = this.Id;
            }

            this.Reset();

            return instance;
        }
    }

}
