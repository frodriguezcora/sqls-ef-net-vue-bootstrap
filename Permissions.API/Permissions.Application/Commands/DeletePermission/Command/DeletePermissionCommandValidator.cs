using FluentValidation;

namespace Permissions.Application.Commands.DeletePermission.Command
{
    public class DeletePermissionCommandValidator : AbstractValidator<DeletePermissionCommand>
    {
        public DeletePermissionCommandValidator()
        {
            RuleFor(x => x.PermissionId).NotEmpty();
        }
    }
}