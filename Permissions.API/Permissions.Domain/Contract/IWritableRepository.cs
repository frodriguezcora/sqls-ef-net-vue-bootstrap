namespace Permissions.Domain.Contract;

public interface IWritableRepository<T> where T : IEntity
{
    T Create(T entity);

    void Update(T entity);

    void Delete(IEntityId entityId);
}
