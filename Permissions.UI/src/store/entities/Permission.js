export default class Permission {

    constructor(
        name,
        surname,
        permissionTypeId,
        requestDate,
        id = null)
    {
        this.name = name
        this.surname = surname
        this.permissionTypeId = permissionTypeId
        this.id = id

        this.setDate(requestDate)
    }

    setDate(date){
        if (date) {
            const dateUnformatted = new Date(date)
            const formattedMonth = dateUnformatted.getMonth()+1 < 10 ? `0${dateUnformatted.getMonth()+1}` : `${dateUnformatted.getMonth()+1}`
            const formattedDay = dateUnformatted.getDate() < 10 ? `0${dateUnformatted.getDate()}` : `${dateUnformatted.getDate()}`
            
            this.requestDate = `${dateUnformatted.getFullYear()}-${formattedMonth}-${formattedDay}`
        }
    }

    static empty() {
        return new Permission('', '', '', '', '')
    }
}