using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Services.Create;

public class CreatePermissonService : ICreatePermissonService
{
    private IWritableRepository<Permission> writablePermissionRepository;

    public CreatePermissonService(IWritableRepository<Permission> writablePermissionRepository)
    {
        this.writablePermissionRepository = writablePermissionRepository;
    }

    public Permission Execute(Permission permission)
    {
        var created = this.writablePermissionRepository.Create(permission);
        return created;
    }
}
