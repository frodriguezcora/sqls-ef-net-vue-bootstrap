﻿namespace Permissions.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        PermissionsDbContext Context { get; }

        void Commit();
    }
}
