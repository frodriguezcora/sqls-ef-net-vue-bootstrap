using FluentValidation;
using MediatR;
using Permissions.Application.Commands.CreatePermission.Command;
using Permissions.Application.Commands.CreatePermission.Response;
using Permissions.Domain.Permission;
using Permissions.Domain.Permission.Builder;
using Permissions.Domain.Permission.Services.Create;

namespace backend_net.Application.Commands.CreatePermission;

public class CreatePermissionCommandHandler : IRequestHandler<CreatePermissionCommand, CreatePermissionResponse>
{
    private readonly IValidator<CreatePermissionCommand> validator;
    private IPermissionBuilder permissionBuilder;
    private ICreatePermissonService createPermissonDomainService;

    public CreatePermissionCommandHandler(
        IValidator<CreatePermissionCommand> validator,
        IPermissionBuilder permissionBuilder,
        ICreatePermissonService createPermissonDomainService)
    {
        this.validator = validator;
        this.permissionBuilder = permissionBuilder;
        this.createPermissonDomainService = createPermissonDomainService;
    }

    public Task<CreatePermissionResponse> Handle(CreatePermissionCommand command, CancellationToken cancellationToken)
    {
        this.validator.ValidateAndThrow(command);

        var instance = this.permissionBuilder
        .SetType(new(command.PermissionTypeId))
        .SetRequestDate(command.RequestDate)
        .SetEmployeeName(command.EmployeeName)
        .SetEmployeeSurname(command.EmployeeSurname)
        .Build();

        var instanceCreated = this.createPermissonDomainService.Execute(instance);

        return Task.FromResult(new CreatePermissionResponse
        {
            Id = instanceCreated.Id.Value
        }); 
    }
}
