using FluentValidation;
using Permissions.Application.Commands.UpdatePermission.Command;

namespace Permissions.Application.Commands.DeletePermission.Command
{
    public class UpdatePermissionCommandValidator : AbstractValidator<UpdatePermissionCommand>
    {
        public UpdatePermissionCommandValidator()
        {
            RuleFor(x => x.PermissionId).NotEmpty();
            RuleFor(x => x.PermissionTypeId).NotEmpty();
            RuleFor(x => x.EmployeeName).NotEmpty();
            RuleFor(x => x.EmployeeSurname).NotEmpty();
            RuleFor(x => x.RequestDate).GreaterThanOrEqualTo(p => DateTime.Today);
        }
    }
}