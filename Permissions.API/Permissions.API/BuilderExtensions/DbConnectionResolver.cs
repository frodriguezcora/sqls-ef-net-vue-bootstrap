﻿using Microsoft.EntityFrameworkCore;
using Permissions.Infrastructure;

namespace Permissions.API.BuilderExtensions
{
    public static class DbConnectionResolver
    {
        public static IServiceCollection ResolveDatabaseConnection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PermissionsDbContext>(options =>
                options.UseSqlServer(configuration["SqlServerConnectionString"]));

            return services;
        }
    }
}
