﻿using Permissions.Domain.Permission.Type;

namespace Permissions.Application.Queries.GetPermissionTypeList.Response
{
    public static class PermissionTypeListResponseMapper
    {
        public static PermissionTypeListResponse Map(IEnumerable<PermissionType> permissions) => new()
        {
            List = permissions.Select(permission => new PermissionTypeDto
            {
                Id = permission.Id.Value,
                Description = permission.Description
            })
        };
    }
}
