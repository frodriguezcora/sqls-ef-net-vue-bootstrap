using Permissions.Domain.Contract;
using Permissions.Domain.Permission.Type;

namespace Permissions.Domain.Permission.Services.GetTypeList;

public class GetPermissionTypeListService : IGetPermissionTypeListService
{
    private IRedableRepository<PermissionType> redablePermissionTypeRepository;

    public GetPermissionTypeListService(IRedableRepository<PermissionType> redablePermissionTypeRepository)
    {
        this.redablePermissionTypeRepository = redablePermissionTypeRepository;
    }

    public IEnumerable<PermissionType> Execute()
    {
        return this.redablePermissionTypeRepository.GetList();
    }
}
