namespace Permissions.Domain.Permission.Services.GetList;

public interface IGetPermissionListService
{
    IEnumerable<Permission> Execute();
}