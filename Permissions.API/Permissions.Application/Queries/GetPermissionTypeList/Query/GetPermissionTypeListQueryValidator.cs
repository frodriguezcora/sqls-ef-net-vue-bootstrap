﻿using FluentValidation;

namespace Permissions.Application.Queries.GetPermissionTypeList.Query
{
    public class GetPermissionTypeListQueryValidator : AbstractValidator<GetPermissionTypeListQuery>
    {
        public GetPermissionTypeListQueryValidator()
        {
        }
    }
}
