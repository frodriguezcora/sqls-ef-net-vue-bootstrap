﻿namespace Permissions.Application.Queries.GetPermissionList.Response
{
    public class PermissionDto
    {
        public int PermissionId { get; set; }

        public int PermissionTypeId { get; set; }

        public string PermissionTypeDescription { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeSurname { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
