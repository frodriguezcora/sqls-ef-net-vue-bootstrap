const HOME = '/'
const PERMISSIONS_CREATE = '/permission'
const PERMISSIONS_EDIT = '/permission/:id'

export {
    HOME,
    PERMISSIONS_EDIT,
    PERMISSIONS_CREATE
}