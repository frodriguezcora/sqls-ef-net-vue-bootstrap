using MediatR;

namespace Permissions.Application.Commands.DeletePermission.Command;

public class DeletePermissionCommand : IRequest
{
    public int PermissionId { get; set; }
}