﻿using System.ComponentModel.DataAnnotations;

namespace Permissions.Infrastructure.Models
{
    public class PermissionTypeModel : IModel
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
