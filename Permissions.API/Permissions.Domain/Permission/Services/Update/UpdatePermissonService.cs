using Permissions.Domain.Contract;

namespace Permissions.Domain.Permission.Services.Update;

public class UpdatePermissonService : IUpdatePermissonService
{
    private IWritableRepository<Permission> writablePermissionRepository;

    public UpdatePermissonService(IWritableRepository<Permission> writablePermissionRepository)
    {
        this.writablePermissionRepository = writablePermissionRepository;
    }

    public void Execute(Permission permission)
    {
        this.writablePermissionRepository.Update(permission);
    }
}
