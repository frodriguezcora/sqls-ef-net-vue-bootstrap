﻿using Microsoft.EntityFrameworkCore;
using Permissions.Domain.Contract;
using Permissions.Domain.Permission;
using Permissions.Infrastructure.Models;
using Permissions.Infrastructure.UnitOfWork;

namespace Permissions.Infrastructure.Repositories.Permissions
{
    public class PermissionRepository : BaseModelRepository<PermissionModel>, IRedableRepository<Permission>, IWritableRepository<Permission>
    {
        public PermissionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Permission Create(Permission entity)
        {
            var model = PermissionRepositoryMapper.MapEntityToModel(entity);
            var newId = this.CreateModel(model);

            entity.Id = new(newId);

            return entity;
        }

        public void Delete(IEntityId entityId)
        {
            this.DeleteModel(entityId.Value);
        }

        public void Update(Permission entity)
        {
            var model = PermissionRepositoryMapper.MapEntityToModel(entity);
            this.UpdateModel(model);
        }

        public Permission GetById(IEntityId entityId)
        {

            var model = this.unitOfWork
                .Context
                .Set<PermissionModel>()
                .Include(model => model.Type)
                .SingleOrDefault(model => model.Id == entityId.Value);

            if (model is null)
            {
                throw new ArgumentNullException($"The Permission {entityId.Value} does not exist");
            }

            return PermissionRepositoryMapper.MapModelToEntity(model);
        }

        public IEnumerable<Permission> GetList()
        {
            var models = this.unitOfWork
                .Context
                .Set<PermissionModel>()
                .Include(model => model.Type)
                .AsNoTracking()
                .AsEnumerable();

            return models.Select(model => PermissionRepositoryMapper.MapModelToEntity(model));
        }
    }
}
