namespace Permissions.Domain.Contract;

public interface IRedableRepository<T> where T : IEntity
{
    T GetById(IEntityId id);

    IEnumerable<T> GetList();
}
