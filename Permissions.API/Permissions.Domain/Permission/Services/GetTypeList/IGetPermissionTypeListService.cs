using Permissions.Domain.Permission.Type;

namespace Permissions.Domain.Permission.Services.GetTypeList;

public interface IGetPermissionTypeListService
{
    IEnumerable<PermissionType> Execute();
}