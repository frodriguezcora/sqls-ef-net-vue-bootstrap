﻿namespace Permissions.API.Middlewares.ErrorHandler
{
    public class ErrorResponse
    {
        public string Message { get; set; }
    }
}
