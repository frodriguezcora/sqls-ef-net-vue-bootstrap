﻿namespace Permissions.Application.Queries.GetPermissionList.Response
{
    public class PermissionListResponse
    {
        public IEnumerable<PermissionDto> List { get; set; }
    }
}