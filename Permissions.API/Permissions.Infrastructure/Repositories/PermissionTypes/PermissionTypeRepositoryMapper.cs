﻿using Permissions.Domain.Permission.Type;
using Permissions.Infrastructure.Models;

namespace Permissions.Infrastructure.Repositories.Permissions
{
    internal static class PermissionTypeRepositoryMapper
    {
        internal static PermissionTypeModel MapEntityToModel(PermissionType type) => new()
        {
            Id = type.Id.Value,
            Description = type.Description
        };

        internal static PermissionType MapModelToEntity(PermissionTypeModel typeModel) => new()
        {
            Id = new(typeModel.Id),
            Description = typeModel.Description
        };
    }
}
