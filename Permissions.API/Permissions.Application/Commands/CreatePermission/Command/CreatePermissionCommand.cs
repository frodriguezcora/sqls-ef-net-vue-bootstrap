using MediatR;
using Permissions.Application.Commands.CreatePermission.Response;

namespace Permissions.Application.Commands.CreatePermission.Command;

public class CreatePermissionCommand : IRequest<CreatePermissionResponse>
{
    public int PermissionTypeId { get; set; }

    public string EmployeeName { get; set; }

    public string EmployeeSurname { get; set; }

    public DateTime RequestDate { get; set; }
}
