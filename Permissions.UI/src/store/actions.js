const LOAD_PERMISSIONS = 'LOAD_PERMISSIONS'
const LOAD_PERMISSION_TYPES = 'LOAD_PERMISSION_TYPES'
const CREATE_PERMISSION = 'CREATE_PERMISSION'
const EDIT_PERMISSION = 'EDIT_PERMISSION'
const REMOVE_PERMISSION = 'REMOVE_PERMISSION'

export {
    LOAD_PERMISSIONS,
    LOAD_PERMISSION_TYPES,
    CREATE_PERMISSION,
    EDIT_PERMISSION,
    REMOVE_PERMISSION
}
