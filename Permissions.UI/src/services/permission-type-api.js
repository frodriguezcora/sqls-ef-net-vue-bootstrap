import httpClient from '@/services'

export default new class PermissionTypeApi {

    constructor() {
        this.basePath = 'permissiontype'
    }

    getList() {
        return httpClient.get(`${this.basePath}/list`)
    }
}