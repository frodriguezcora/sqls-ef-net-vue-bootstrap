﻿namespace Permissions.API.BuilderExtensions
{
    public static class CorsConfigurator
    {
        public static WebApplication UseCustomCors(this WebApplication app)
        {

            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed((host) => true)
                .AllowCredentials()
            );

            return app;
        }
    }
}
