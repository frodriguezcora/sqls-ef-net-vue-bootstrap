namespace Permissions.Domain.Permission.Services.Create;

public interface ICreatePermissonService
{
    Permission Execute(Permission permission);
}