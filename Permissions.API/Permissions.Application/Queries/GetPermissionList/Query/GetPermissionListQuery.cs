﻿using MediatR;
using Permissions.Application.Queries.GetPermissionList.Response;

namespace Permissions.Application.Queries.GetPermissionList.Query
{
    public class GetPermissionListQuery : IRequest<PermissionListResponse>
    {
    }
}
