import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

import { HOME, PERMISSIONS_CREATE, PERMISSIONS_EDIT } from './paths'

Vue.use(VueRouter)

const routes = [
    {
      path: HOME,
      name: 'Home',
      component: Home
    },
    {
      path: PERMISSIONS_CREATE,
      name: 'Create Permission',
      component: () => import('@/views/PermissionCreate.vue')
    },
    {
      path: PERMISSIONS_EDIT,
      name: 'Edit Permission',
      component: () => import('@/views/PermissionEdit.vue')
    },
]
  
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
}) 

export default router