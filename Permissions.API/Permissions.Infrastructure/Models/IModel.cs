﻿namespace Permissions.Infrastructure.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}
