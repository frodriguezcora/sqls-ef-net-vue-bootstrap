﻿using MediatR;
using Permissions.Application.Queries.GetPermissionTypeList.Response;

namespace Permissions.Application.Queries.GetPermissionTypeList.Query
{
    public class GetPermissionTypeListQuery : IRequest<PermissionTypeListResponse>
    {
    }
}
