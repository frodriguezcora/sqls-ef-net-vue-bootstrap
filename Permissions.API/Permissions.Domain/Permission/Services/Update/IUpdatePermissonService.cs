namespace Permissions.Domain.Permission.Services.Update;

public interface IUpdatePermissonService
{
    void Execute(Permission permission);
}