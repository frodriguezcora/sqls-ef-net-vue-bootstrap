﻿using FluentValidation;

namespace Permissions.Application.Queries.GetPermissionList.Query
{
    public class GetPermissionListQueryValidator : AbstractValidator<GetPermissionListQuery>
    {
        public GetPermissionListQueryValidator()
        {
        }
    }
}
