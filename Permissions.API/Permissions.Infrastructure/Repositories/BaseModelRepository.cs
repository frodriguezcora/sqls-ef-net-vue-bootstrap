﻿using Microsoft.EntityFrameworkCore;
using Permissions.Infrastructure.Models;
using Permissions.Infrastructure.UnitOfWork;

namespace Permissions.Infrastructure.Repositories
{
    public abstract class BaseModelRepository<T> where T : class
    {
        protected IUnitOfWork unitOfWork;

        protected BaseModelRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        protected T GetModelById(int id)
        {
            return this.FindModelById(id);
        }

        protected IEnumerable<T> GetModelList()
        {
            return this.unitOfWork
                .Context
                .Set<T>()
                .AsNoTracking()
                .AsEnumerable();
        }

        protected int CreateModel(T model)
        {
            this.unitOfWork
                .Context
                .Add(model);

            this.unitOfWork.Commit();

            return ((IModel)model).Id;
        }

        protected void UpdateModel(T model)
        {
            this.unitOfWork
                .Context
                .Update(model);
        }

        protected void DeleteModel(int id)
        {
            var model = this.FindModelById(id);
            this.unitOfWork
                .Context
                .Set<T>()
                .Remove(model);
        }

        private T FindModelById(int modelId)
        {
            var model = this.unitOfWork
                .Context.Set<T>()
                .SingleOrDefault(model => ((IModel) model).Id == modelId);

            if (model is null)
            {
                throw new ArgumentNullException($"{nameof(T)} {modelId} does not exist");
            }

            return model;
        }
    }
}
